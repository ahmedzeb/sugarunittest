describe('Shortcuts', function() {
    var app,
        view,
        mousetrapBindStub,
        mousetrapUnbindStub,
        View = Backbone.View;

    beforeEach(function() {
        app = SugarTest.app;

        view = new View();

        mousetrapBindStub = sinon.stub();
        mousetrapUnbindStub = sinon.stub();

        Mousetrap = {
            bind: mousetrapBindStub,
            unbind: mousetrapUnbindStub
        };
    });

    afterEach(function() {
        app.shortcuts._currentSession = null;
        app.shortcuts._savedSessions = [];
        app.shortcuts._globalShortcuts = {};
        app.shortcuts._enable = false;
        Mousetrap = undefined;
    });

    describe('createSession', function() {
        it('should create a new shortcut session', function() {
            app.shortcuts.createSession(['foo'], view);
            expect(app.shortcuts.getCurrentSession()).toBeDefined();
        });

        it('should activate the new session', function() {
            app.shortcuts.createSession(['foo'], view);
            expect(app.shortcuts.getCurrentSession().isActive()).toBe(true);
        });

        it('should deactivate the previous session', function() {
            var deactivateSpy;

            app.shortcuts.createSession(['foo'], view);
            deactivateSpy = sinon.spy(app.shortcuts.getCurrentSession(), 'deactivate');

            app.shortcuts.createSession(['foo'], view);
            expect(deactivateSpy.calledOnce).toBe(true)

            deactivateSpy.restore();
        });
    });

    describe('clearSession', function() {
        it('should deactivate the current session', function() {
            var shortcutSession;

            app.shortcuts.createSession(['foo'], view);
            expect(app.shortcuts.getCurrentSession().isActive()).toBe(true);

            shortcutSession = app.shortcuts.getCurrentSession();
            app.shortcuts.clearSession();
            expect(shortcutSession.isActive()).toBe(false);
        });

        it('should not have any sessions the current session', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.clearSession();
            expect(app.shortcuts.getCurrentSession()).toBeFalsy();
        });
    });

    describe('register', function() {
        it('should bind shortcut keys if it is allowed in the session', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register('foo', 'a', $.noop, view);
            expect(mousetrapBindStub.calledOnce).toBe(true);
        });

        it('should bind all shortcut keys when multiple keys are specified', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register('foo', ['a','b'], $.noop, view);
            expect(mousetrapBindStub.args[0][0]).toEqual(['a','b']);
        });

        it('should not bind shortcut keys if the session is inactive', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.getCurrentSession().deactivate();
            app.shortcuts.register('foo', 'a', $.noop, view);
            expect(mousetrapBindStub.called).toBe(false);
        });

        it('should register shortcut keys if it is allowed in the session', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register('foo', 'a', $.noop, view);
            expect(app.shortcuts.getCurrentSession()._shortcuts.foo.keys).toBeDefined();
        });

        it('should register shortcut keys even if the session is inactive', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.getCurrentSession().deactivate();
            app.shortcuts.register('foo', 'a', $.noop, view);
            expect(app.shortcuts.getCurrentSession()._shortcuts.foo.keys).toBeDefined();
        });

        it('should not register shortcut keys if they are not allowed in the session', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register('bar', 'a', $.noop, view);
            expect(app.shortcuts.getCurrentSession()._shortcuts.bar).toBeUndefined();
            expect(mousetrapBindStub.called).toBe(false);
        });

        it('should register global shortcut keys', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register(app.shortcuts.GLOBAL + 'foo', 'a', $.noop, view);
            expect(mousetrapBindStub.calledOnce).toBe(true);
        });

        it('should register global shortcut keys even if the current session is deactivated', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.getCurrentSession().deactivate();
            app.shortcuts.register(app.shortcuts.GLOBAL + 'foo', 'a', $.noop, view);
            expect(mousetrapBindStub.calledOnce).toBe(true);
        });

        it('should register the shortcut for the session that is tied to the component layout', function() {
            var view2 = new View();

            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.saveSession();
            app.shortcuts.createSession(['foo'], view2);

            app.shortcuts.register('foo', 'a', $.noop, view);
            expect(app.shortcuts._savedSessions[0]._shortcuts.foo.keys).toBeDefined();
            expect(app.shortcuts.getCurrentSession()._shortcuts.foo.keys).toBeUndefined();
        });

        it('should not register shortcut keys if the component is a dashlet', function() {
            view.layout = new View({
                type: 'dashlet'
            });
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register('bar', 'a', $.noop, view);
            expect(app.shortcuts.getCurrentSession()._shortcuts.bar).toBeUndefined();
            expect(mousetrapBindStub.called).toBe(false);
        });

        it('should register shortcut keys if the same key has already been bound', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register('foo', 'a', $.noop, view);
            app.shortcuts.register('foo', 'a', $.noop, view);
            expect(mousetrapBindStub.calledTwice).toBe(true);
        });

        it('should unregister shortcut keys if the same key has already been bound', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register('foo', 'a', $.noop, view);
            app.shortcuts.register('foo', 'a', $.noop, view);
            expect(mousetrapUnbindStub.calledOnce).toBe(true);
        });

        it('should not register shortcut keys if the same key has already been bound as a global shortcut key', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register(app.shortcuts.GLOBAL + 'bar', 'a', $.noop, view);
            expect(mousetrapBindStub.calledOnce).toBe(true);

            app.shortcuts.register('foo', 'a', $.noop, view);
            expect(mousetrapBindStub.calledOnce).toBe(true);
        });
    });

    describe('unregister', function() {
        it('should unregister the shortcut', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register('foo', 'a', $.noop, view);
            expect(app.shortcuts.getCurrentSession()._shortcuts.foo.keys).toBeDefined();

            app.shortcuts.unregister('foo', view);
            expect(app.shortcuts.getCurrentSession()._shortcuts.foo.keys).toBeUndefined();
        });

        it('should unregister the shortcut for the session that is tied to the component layout', function() {
            var view2 = new View();

            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.saveSession();
            app.shortcuts.createSession(['foo'], view2);
            app.shortcuts.register('foo', 'a', $.noop, view);
            app.shortcuts.register('foo', 'a', $.noop, view2);
            expect(app.shortcuts._savedSessions[0]._shortcuts.foo.keys).toBeDefined();
            expect(app.shortcuts.getCurrentSession()._shortcuts.foo.keys).toBeDefined();

            app.shortcuts.unregister('foo', view);
            expect(app.shortcuts._savedSessions[0]._shortcuts.foo.keys).toBeUndefined();
            expect(app.shortcuts.getCurrentSession()._shortcuts.foo.keys).toBeDefined();
        });

        it('should unregister but not unbind the shortcut if the session is inactive', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register('foo', 'a', $.noop, view);
            app.shortcuts.getCurrentSession().deactivate();
            expect(mousetrapUnbindStub.calledOnce).toBe(true);

            app.shortcuts.unregister('foo', view);
            expect(app.shortcuts.getCurrentSession()._shortcuts.foo.keys).toBeUndefined();
            expect(mousetrapUnbindStub.calledOnce).toBe(true);
        });

        it('should not unregister if it has never been registered', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.unregister('foo', view);
            expect(mousetrapUnbindStub.called).toBe(false);
        });
    });

    describe('saveSession and restoreSession', function() {
        var view2;

        beforeEach(function() {
            view2 = new View();
        });

        it('should restore all shortcut bindings', function() {
            app.shortcuts.createSession(['a','b'], view);
            app.shortcuts.register('a', 'a', $.noop, view);
            app.shortcuts.register('b', 'b', $.noop, view);
            expect(mousetrapBindStub.callCount).toBe(2);

            app.shortcuts.saveSession();
            app.shortcuts.createSession(['foo'], view2);
            app.shortcuts.restoreSession();

            expect(mousetrapBindStub.callCount).toBe(4);
        });

        it('should make the last save session active', function() {
            var firstSession;

            app.shortcuts.createSession(['foo'], view);

            firstSession = app.shortcuts.getCurrentSession();
            expect(firstSession.isActive()).toBe(true);

            app.shortcuts.saveSession();
            app.shortcuts.createSession(['bar'], view2);

            expect(firstSession.isActive()).toBe(false);

            app.shortcuts.restoreSession();

            expect(firstSession.isActive()).toBe(true);
            expect(app.shortcuts.getCurrentSession()).toBe(firstSession);
        });

        it('should not restore shortcut session that is tied to a disposed layout', function() {
            var firstSession;

            app.shortcuts.createSession(['foo'], view);

            firstSession = app.shortcuts.getCurrentSession();

            app.shortcuts.saveSession();
            app.shortcuts.createSession(['foo'], view2);
            app.shortcuts.saveSession();
            app.shortcuts.createSession(['foo'], new View());

            view2.disposed = true;
            app.shortcuts.restoreSession();

            expect(app.shortcuts.getCurrentSession()).toBe(firstSession);
            expect(firstSession.isActive()).toBe(true);
        });

        it('should not restore shortcut session when there are no saved sessions to restore', function() {
            var firstSession;
            app.shortcuts.createSession(['foo'], view);
            firstSession = app.shortcuts.getCurrentSession();
            app.shortcuts.restoreSession();
            expect(app.shortcuts.getCurrentSession()).toBe(firstSession);
        });

        it('should have no shortcut session if the last session had none', function() {
            app.shortcuts.saveSession();
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.saveSession();
            app.shortcuts.createSession(['foo'], view2);

            app.shortcuts.restoreSession();
            app.shortcuts.restoreSession();
            expect(app.shortcuts.getCurrentSession()).toBeNull();
        });
    });

    describe('_getShortcutSessionForComponent', function() {
        it('should get the shortcut session that the component is tied to', function() {
            var result, session,
                view2 = new View();

            view2.layout = view;
            app.shortcuts.createSession(['foo'], view);
            session = app.shortcuts.getCurrentSession();
            result = app.shortcuts._getShortcutSessionForComponent(view2);

            expect(result).toBe(session);
        });

        it('should get the shortcut session even if the session is saved and not active', function() {
            var result, session,
                view2 = new View();

            view2.layout = view;
            app.shortcuts.createSession(['foo'], view);
            session = app.shortcuts.getCurrentSession();
            app.shortcuts.saveSession();
            app.shortcuts.createSession(['foo'], new View());

            result = app.shortcuts._getShortcutSessionForComponent(view2);

            expect(result).toBe(session);
        });

        it('should return undefined when it cannot find the shortcut session', function() {
            var result;

            app.shortcuts.createSession(['foo'], view);
            result = app.shortcuts._getShortcutSessionForComponent(new View());

            expect(result).toBeUndefined();
        });
    });

    describe('deleteSavedSession', function() {
        it('should remove the session that the layout is tied to', function() {
            var firstSession, secondSession;

            app.shortcuts.createSession(['foo'], view);
            firstSession = app.shortcuts.getCurrentSession();

            app.shortcuts.saveSession();
            app.shortcuts.createSession(['foo'], new View());
            secondSession = app.shortcuts.getCurrentSession();

            app.shortcuts.saveSession();
            app.shortcuts.createSession(['foo'], new View());

            expect(app.shortcuts._savedSessions.length).toBe(2);
            expect(app.shortcuts._savedSessions[0]).toBe(firstSession);
            expect(app.shortcuts._savedSessions[1]).toBe(secondSession);

            app.shortcuts.deleteSavedSession(view);
            expect(app.shortcuts._savedSessions.length).toBe(1);
            expect(app.shortcuts._savedSessions[0]).toBe(secondSession);
        });
    });

    describe('getRegisteredGlobalShortcuts', function() {
        it('should return the IDs and the keys of all available global shortcuts', function() {
            app.shortcuts.createSession([], view);
            app.shortcuts.register(app.shortcuts.GLOBAL + 'foo', 'a', $.noop, view);
            app.shortcuts.register(app.shortcuts.GLOBAL + 'bar', 'b', $.noop, view);

            expect(app.shortcuts.getRegisteredGlobalShortcuts()).toEqual([{
                id: app.shortcuts.GLOBAL + 'foo',
                keys: ['a']
            }, {
                id: app.shortcuts.GLOBAL + 'bar',
                keys: ['b']
            }]);
        });
    });

    describe('shouldCallOnFocus', function() {
        it('should return true if the key has been registered to be called on focus', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register('foo', 'a', $.noop, view, true);

            expect(app.shortcuts.shouldCallOnFocus('a')).toBe(true);
        });

        it('should return false if the key has been registered to not call on focus', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register('foo', 'a', $.noop, view);

            expect(app.shortcuts.shouldCallOnFocus('a')).toBe(false);
        });

        it('should return true if a global shortcut key has been registered to be called on focus', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register(app.shortcuts.GLOBAL + 'foo', 'a', $.noop, view, true);

            expect(app.shortcuts.shouldCallOnFocus('a')).toBe(true);
        });

        it('should return false if the session is not active', function() {
            app.shortcuts.createSession(['foo'], view);
            app.shortcuts.register('foo', 'a', $.noop, view, true);
            app.shortcuts.getCurrentSession().deactivate();

            expect(app.shortcuts.shouldCallOnFocus('a')).toBe(false);
        });

        it('should return false if the key has been not registered', function() {
            app.shortcuts.createSession([], view);
            expect(app.shortcuts.shouldCallOnFocus('a')).toBe(false);
        });

        it('should return false if there are no sessions', function() {
            expect(app.shortcuts.shouldCallOnFocus('a')).toBe(false);
        });
    });

    describe('ShortcutSession.getRegisteredShortcuts', function() {
        var session,
            expected;
        beforeEach(function() {
            expected = [{
                id: 'foo',
                keys: ['a']
            }, {
                id: 'bar',
                keys: ['b']
            }];

            app.shortcuts.createSession(['foo','bar', 'test'], view);
            app.shortcuts.register('foo', 'a', $.noop, view);
            app.shortcuts.register('bar', 'b', $.noop, view);
            session = app.shortcuts.getCurrentSession();
        });

        afterEach(function() {
            session = null;
        });

        it('should return the IDs and the keys of all registered shortcuts for that session', function() {
            expect(session.getRegisteredShortcuts()).toEqual(expected);
        });

        it('should not return shortcuts that do not have keys registered', function() {
            var testObjFound = _.find(session.getRegisteredShortcuts(), function(shortcut) {
                return shortcut.id == 'test';
            });
            expect(testObjFound).toBeUndefined();
        });
    });

    describe('enable/disable', function() {
        it('should enable shortcuts if enable is called', function() {
            app.shortcuts.enable();
            expect(app.shortcuts.isEnabled()).toBe(true);
        });

        it('should disable shortcuts if disable is called', function() {
            app.shortcuts.disable();
            expect(app.shortcuts.isEnabled()).toBe(false);
        });
    });
});

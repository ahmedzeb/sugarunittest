describe('app.cache', function () {
    var app;

    beforeEach(function () {
        app = SugarTest.app; // from spec-helper
        app.cache.store = stash;
        app.cache.cutAll();
    });

    afterEach(function () {
        app.cache.cutAll();
    });

    it('should store strings', function () {
        var value = "This is a test.",
            key = "testKey";
        app.cache.set(key, value);
        expect(app.cache.get(key)).toEqual(value);
    });

    it('should store objects', function () {
        var value = {foo: "test", bar:{more:"a"}},
            key = "testKey";
        app.cache.set(key, value);
        expect(app.cache.get(key)).toEqual(value);
    });

    it('should store functions', function () {
        var func = function(){return "Hello World";},
            key = "testKey";
        app.cache.set(key, func);
        expect(app.cache.get(key)()).toEqual(func());
    });

    it('should store DOM elements', function () {
        var el = document.createElement("div"), key;
        el.id = "testID";
        el.className = "Test";
        key = "testKey";
        app.cache.set(key, el);
        //Ensure it is an element
        expect(app.cache.get(key) instanceof HTMLElement).toBeTruthy();
        //And it has all the expected properties
        expect(app.cache.get(key).id).toEqual(el.id);
        expect(app.cache.get(key).className).toEqual(el.className);

    });

    it('should append values', function () {
        var value = "Hello",
            key = "testKey";
        app.cache.set(key, value);
        expect(app.cache.get(key)).toEqual(value);

        app.cache.add(key, " World");
        expect(app.cache.get(key)).toEqual("Hello World");
    });


    it('should remove values', function () {
        var value = "Hello",
            key = "testKey";
        app.cache.set(key, value);
        expect(app.cache.get(key)).toEqual(value);

        app.cache.cut(key);
        expect(app.cache.get(key)).toBeFalsy();
    });

    it('should provide has to determine if key exists', function () {
        var value = "Hello",
            key = "testKey";
        app.cache.set(key, value);
        app.cache.cut(key);
        expect(app.cache.has(key)).toBeFalsy();
    });

    it('should provide has to determine if key exists for a different store', function () {
        var mystore = {
            has: _.noop,
            getAll: _.noop,
        };
        app.cache.store = mystore;

        var spy = sinon.spy(mystore, 'has');

        app.cache.has('test');

        expect(spy).toHaveBeenCalled();

        spy.restore();
    });

    it('should remove all values', function () {
        var value = "Hello",
            key = "testKey",
            key2 = "testKey2";
        app.cache.set(key, value);
        app.cache.set(key2, value);
        expect(app.cache.get(key)).toEqual(value);
        expect(app.cache.get(key2)).toEqual(value);

        app.cache.cutAll();
        expect(app.cache.get(key)).toBeFalsy();
        expect(app.cache.get(key2)).toBeFalsy();
    });

    it('should clean up unimportant values when clean is called', function() {
        var k1 = "notImportant",
            k2 = "important",
            callback =  function(cb) {
                cb([k2]);
            };

        app.cache.on("cache:clean", callback);

        app.cache.set(k1, "foo");
        app.cache.set(k2, "bar");

        app.cache.clean();

        expect(app.cache.get(k1)).toBeUndefined()
        expect(app.cache.get(k2)).toEqual("bar");

        app.cache.off("cache:clean", callback);
    });

    it('should call clean when a quota error occurs', function() {
        var e = {name: "QUOTA_EXCEEDED_ERR"},
            spy = sinon.spy(app.cache, "clean"),
            set = sinon.stub(stash, "set").throws(e);

        expect(function(){app.cache.set("foo", "bar")}).toThrow(e);
        expect(spy).toHaveBeenCalled();
        spy.restore();
        set.restore();
    });

    it('should noop when some other error occurs', function() {
        var e = {name: 'RANDOM_ERR'},
            spy = sinon.spy(app.cache, 'clean'),
            set = sinon.stub(stash, 'set').throws(e);

        app.cache.set('foo', 'bar');
        expect(spy).not.toHaveBeenCalled();
        spy.restore();
        set.restore();
    });

    it('should call clean when a quota error occurs on add', function() {
        var e = {name: 'QUOTA_EXCEEDED_ERR'},
            spy = sinon.spy(app.cache, 'clean'),
            set = sinon.stub(stash, 'add').throws(e);

        expect(function(){app.cache.add('foo', 'bar')}).toThrow(e);
        expect(spy).toHaveBeenCalled();
        spy.restore();
        set.restore();
    });

    it('should noop when some other error occurs on add', function() {
        var e = {name: 'RANDOM_ERR'},
            spy = sinon.spy(app.cache, 'clean'),
            set = sinon.stub(stash, 'add').throws(e);

        app.cache.add('foo', 'bar');
        expect(spy).not.toHaveBeenCalled();
        spy.restore();
        set.restore();
    });
});

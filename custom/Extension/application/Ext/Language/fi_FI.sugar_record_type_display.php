<?php
 // created: 2017-02-28 13:00:58

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Jäsen',
  'Opportunities' => 'Myyntimahdollisuus',
  'Cases' => 'Palvelupyyntö',
  'Leads' => 'Liidi',
  'Contacts' => 'Kontaktit',
  'Products' => 'Tarjottu tuoterivi',
  'Quotes' => 'Tarjous',
  'Bugs' => 'Bugi',
  'Project' => 'Projekti',
  'Prospects' => 'Tavoite',
  'ProjectTask' => 'Projektitehtävä',
  'Tasks' => 'Tehtävä',
  'KBContents' => 'Tietämyskanta',
  'RevenueLineItems' => 'Tuoterivit',
);
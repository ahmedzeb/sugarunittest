<?php
 // created: 2017-02-28 13:00:58

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Jäsen',
  'Contacts' => 'Yhteystiedot',
  'Tasks' => 'Tehtävä',
  'Opportunities' => 'Myyntimahdollisuus',
  'Products' => 'Tarjottu tuoterivi',
  'Quotes' => 'Tarjous',
  'Bugs' => 'Bugit',
  'Cases' => 'Palvelupyyntö',
  'Leads' => 'Liidi',
  'Project' => 'Projekti',
  'ProjectTask' => 'Projektitehtävä',
  'Prospects' => 'Tavoite',
  'KBContents' => 'Tietämyskanta',
  'RevenueLineItems' => 'Tuoterivit',
);
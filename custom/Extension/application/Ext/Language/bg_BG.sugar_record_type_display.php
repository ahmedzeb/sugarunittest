<?php
 // created: 2017-02-28 13:00:49

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Организация',
  'Opportunities' => 'Възможност',
  'Cases' => 'Казус',
  'Leads' => 'Потенциален клиент',
  'Contacts' => 'Контакти',
  'Products' => 'Офериран продукт',
  'Quotes' => 'Оферта',
  'Bugs' => 'Проблем',
  'Project' => 'Проекти',
  'Prospects' => 'Целеви клиент',
  'ProjectTask' => 'Задача по проект',
  'Tasks' => 'Задача',
  'KBContents' => 'База от знания',
  'RevenueLineItems' => 'Приходни позиции',
);
<?php
 // created: 2017-02-28 13:00:52

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Azienda',
  'Contacts' => 'Contatto',
  'Tasks' => 'Compito',
  'Opportunities' => 'Opportunità',
  'Products' => 'Prodotto',
  'Quotes' => 'Offerta',
  'Bugs' => 'Bug',
  'Cases' => 'Reclamo',
  'Leads' => 'Lead',
  'Project' => 'Progetto',
  'ProjectTask' => 'Compiti di Progetto',
  'Prospects' => 'Obiettivo',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Elementi dell´Opportunità',
);
<?php
 // created: 2017-02-28 13:00:50

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Firma',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Aufgabe',
  'Opportunities' => 'Verkaufschance',
  'Products' => 'Produkt',
  'Quotes' => 'Angebot',
  'Bugs' => 'Fehler',
  'Cases' => 'Ticket',
  'Leads' => 'Interessent',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektaufgabe',
  'Prospects' => 'Zielkontakt',
  'KBContents' => 'Wissensdatenbank',
  'RevenueLineItems' => 'Umsatzposten',
);
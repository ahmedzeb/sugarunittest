<?php
 // created: 2017-02-28 13:00:57

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Konto',
  'Opportunities' => 'Võimalus',
  'Cases' => 'Juhtum',
  'Leads' => 'Müügivihje',
  'Contacts' => 'Kontaktid',
  'Products' => 'Pakkumuse artikkel',
  'Quotes' => 'Pakkumus',
  'Bugs' => 'Viga',
  'Project' => 'Projekt',
  'Prospects' => 'Eesmärk',
  'ProjectTask' => 'Projekti ülesanne',
  'Tasks' => 'Ülesanne',
  'KBContents' => 'Teadmusbaas',
  'RevenueLineItems' => 'Tuluartiklid',
);
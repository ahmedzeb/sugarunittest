<?php
 // created: 2017-02-28 13:00:53

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Uzņēmums',
  'Opportunities' => 'Iespēja',
  'Cases' => 'Pieteikums',
  'Leads' => 'Interesents',
  'Contacts' => 'Kontaktpersonas',
  'Products' => 'Piedāvājuma rinda',
  'Quotes' => 'Piedāvājums',
  'Bugs' => 'Kļūda',
  'Project' => 'Projekts',
  'Prospects' => 'Mērķis',
  'ProjectTask' => 'Projekta uzdevums',
  'Tasks' => 'Uzdevums',
  'KBContents' => 'Zināšanu bāze',
  'RevenueLineItems' => 'Ieņēmumu posteņi',
);
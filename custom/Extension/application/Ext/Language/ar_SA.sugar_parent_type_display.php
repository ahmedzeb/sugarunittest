<?php
 // created: 2017-02-28 13:00:58

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'الحساب',
  'Contacts' => 'جهة الاتصال',
  'Tasks' => 'المهمة',
  'Opportunities' => 'الفرصة',
  'Products' => 'البند المسعر',
  'Quotes' => 'عرض السعر',
  'Bugs' => 'الأخطاء',
  'Cases' => 'الحالة',
  'Leads' => 'العميل المتوقع',
  'Project' => 'المشروع',
  'ProjectTask' => 'مهمة المشروع',
  'Prospects' => 'الهدف',
  'KBContents' => 'قاعدة المعارف',
  'RevenueLineItems' => 'بنود العائدات',
);
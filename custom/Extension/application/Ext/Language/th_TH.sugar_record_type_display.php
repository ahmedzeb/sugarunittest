<?php
 // created: 2017-02-28 13:00:55

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'บัญชี',
  'Opportunities' => 'โอกาสทางการขาย',
  'Cases' => 'เคส',
  'Leads' => 'ผู้สนใจ',
  'Contacts' => 'ที่อยู่ติดต่อ',
  'Products' => 'รายการบรรทัดการเสนอราคา',
  'Quotes' => 'การเสนอราคา',
  'Bugs' => 'บัก',
  'Project' => 'โครงการ',
  'Prospects' => 'เป้าหมาย',
  'ProjectTask' => 'งานของโครงการ',
  'Tasks' => 'งาน',
  'KBContents' => 'ฐานความรู้',
  'RevenueLineItems' => 'รายการบรรทัดรายได้',
);
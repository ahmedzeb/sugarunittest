<?php
 // created: 2017-02-28 13:00:55

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Müşteri',
  'Opportunities' => 'Fırsat',
  'Cases' => 'Talep',
  'Leads' => 'Potansiyel',
  'Contacts' => 'Kontaklar',
  'Products' => 'Teklif Kalemi',
  'Quotes' => 'Teklif',
  'Bugs' => 'Hata',
  'Project' => 'Proje',
  'Prospects' => 'Hedef',
  'ProjectTask' => 'Proje Görevi',
  'Tasks' => 'Görev',
  'KBContents' => 'Bilgi Tabanı',
  'RevenueLineItems' => 'Gelir Kalemleri',
);
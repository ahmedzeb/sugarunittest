<?php
 // created: 2017-02-28 13:00:51

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Kliens',
  'Opportunities' => 'Lehetőség',
  'Cases' => 'Eset',
  'Leads' => 'Ajánlás',
  'Contacts' => 'Kapcsolatok',
  'Products' => 'Megajánlott Tétel',
  'Quotes' => 'Árajánlat',
  'Bugs' => 'Hiba',
  'Project' => 'Projekt',
  'Prospects' => 'Cél',
  'ProjectTask' => 'Projektfeladat',
  'Tasks' => 'Feladat',
  'KBContents' => 'Tudásbázis',
  'RevenueLineItems' => 'Bevétel sorok',
);
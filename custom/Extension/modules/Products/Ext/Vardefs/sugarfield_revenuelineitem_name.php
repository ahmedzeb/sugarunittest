<?php
 // created: 2017-02-28 13:00:49
$dictionary['Product']['fields']['revenuelineitem_name']['audited']=false;
$dictionary['Product']['fields']['revenuelineitem_name']['massupdate']=true;
$dictionary['Product']['fields']['revenuelineitem_name']['duplicate_merge']='enabled';
$dictionary['Product']['fields']['revenuelineitem_name']['duplicate_merge_dom_value']=1;
$dictionary['Product']['fields']['revenuelineitem_name']['merge_filter']='disabled';
$dictionary['Product']['fields']['revenuelineitem_name']['calculated']=false;
$dictionary['Product']['fields']['revenuelineitem_name']['studio']='visible';

 ?>